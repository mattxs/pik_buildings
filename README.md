### Сервис для ведения учёта положенных в доме кирпичей.

Запуск сервиса

- `requirements.txt` - установить записимости
- `npm install react` - установить React
- `npm install antd` - установить библиотеку Ant.design
- `app.py` - Запустить backend на localhost:5000
- `npm script start` - Запустить frontend на localhost:3000
