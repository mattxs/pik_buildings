from datetime import datetime

from flask import Flask, request, jsonify
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
CORS(app)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///building.db'
db = SQLAlchemy(app)


class Build(db.Model):
    __tablename__ = 'build'

    def __init__(self, address, date_of_build):
        self.address = address
        self.date_of_build = date_of_build

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    address = db.Column(db.VARCHAR(), unique=True)
    date_of_build = db.Column(db.DATE)
    bricks = db.relationship('Brick', backref='build', lazy=True)

    def __repr__(self):
        return f'Адрес дома: {self.address}, построен: {self.date_of_build}.'


class Brick(db.Model):
    __tablename__ = 'brick'

    def __init__(self, build_id, bricks):
        self.build_id = build_id
        self.bricks_count = bricks

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    bricks_count = db.Column(db.Integer)
    build_id = db.Column(db.Integer, db.ForeignKey(Build.id))

    def __repr__(self):
        return f'Количество кирпичиков {self.bricks_count}'


@app.route('/building', methods=['POST'])
def create_building():
    date_format = '%Y-%m-%d'
    if request.method == 'POST':
        data = request.get_json(force=True)
        address = data['address']
        date = data['date_of_build'] if data['date_of_build'] is not None else datetime.now()
        if date == '':
            date = datetime.now()
        date_of_build = datetime.strptime(date, date_format) if isinstance(date, str) else date
        print(date, address)
        db.session.add(Build(address, date_of_build))
        db.session.commit()
        return f'OK --> {address}, {date_of_build}'
    else:
        return 'NOT OK'


@app.route('/addresses')
def get_addresses():
    address = db.session.query(Build.address).all()
    return jsonify([i.address for i in address])


@app.route('/building/add-bricks', methods=['POST'])
def put_bricks():
    if request.method == 'POST':
        data = request.get_json(force=True)
        address = data['address']
        bricks = int(data['bricks'])
        print(address, bricks)

        home_id = db.session.query(Build.id).filter(Build.address == address).first()
        print(home_id.id)
        db.session.add(Brick(home_id.id, bricks))
        db.session.commit()
    return 'ok'


@app.route('/stats')
def stats():
    stat_data = db.session.query(Build.address, Build.date_of_build, Brick.bricks_count).outerjoin(
        Brick, Brick.build_id == Build.id).all()
    return jsonify([
        {
            'key': key,
            'address': data.address,
            'bricks': data.bricks_count if data.bricks_count is not None else 0,
            'date_of_build': data.date_of_build
        } for key, data in enumerate(stat_data)
    ])


db.create_all()

if __name__ == '__main__':
    app.run(host='localhost:5000', debug=True)
