import React, {Component} from 'react';
import "antd/dist/antd.css";
import './App.css';
import {Col, Layout, Row} from "antd";
import Building from "./components/Building";
import Bricks from "./components/Bricks";
import Stats from "./components/Stats";

class App extends Component {

    state = {
        addresses: [],
        stat: []
    };

    getAddresses = () => {
        fetch('http://localhost:5000/addresses')
            .then(resp => resp.json())
            .then(resp => this.setState({addresses: resp}))
    };

    getBuildStatistic = () => {
        fetch('http://localhost:5000/stats')
            .then(resp => resp.json())
            .then(resp => this.setState({stat: resp}))
    };

    componentDidMount() {
        this.getAddresses();
        this.getBuildStatistic()
    }

    render() {
        const {Content} = Layout;
        const {addresses, stat} = this.state;

        return (
            <div className="App">
                <Layout>
                    <Content>
                        <Row>
                            <Col span={1}/>
                            <Col span={22}>
                                <Building/>
                                <Bricks addresses={addresses}/>
                                <Stats dataSource={stat}/>
                            </Col>
                            <Col span={1}/>
                        </Row>
                    </Content>
                </Layout>
            </div>
        );
    }
}

export default App;
