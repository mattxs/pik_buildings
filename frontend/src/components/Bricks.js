import {Button, Form, Icon, Input, Row, Select} from 'antd';
import React, {useState} from 'react';


export default function Bricks(props) {
    const {addresses} = props;
    const [homeAddress, handleHomeAddress] = useState('');
    console.log("ADR", addresses)

    function handleBricks(e) {
            e.preventDefault();
            let bricks = e.target.bricks.value;
            console.log(bricks, homeAddress)
            fetch('http://localhost:5000/building/add-bricks', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: 'POST',
                body: JSON.stringify({'bricks': bricks, 'address': homeAddress}),
                mode: 'no-cors'
            })
                .then(resp => resp.json())
                .then(resp => console.log(resp.json()))
    }

    function onChange(value) {
        console.log(`selected ${value}`);
        handleHomeAddress(value);
    }

    const {Option} = Select;

    return (
        <Row>
            <Form layout="inline" onSubmit={handleBricks}>
                <Form.Item required>
                    <Select
                        showSearch
                        style={{width: 200}}
                        placeholder="Введите адрес"
                        optionFilterProp="children"
                        onChange={onChange}
                        filterOption={(input, option) =>
                            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                    >
                        {
                            addresses.map((item, index) =>
                                <Option key={index} value={item}>{item}</Option>
                            )}
                    </Select>
                </Form.Item>
                <Form.Item required>
                    <Input
                        name="bricks"
                        type="number"
                        prefix={<Icon type="home" style={{color: 'rgba(0,0,0,.25)'}}/>}
                        placeholder="Количество кирпичей"
                    />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        Накласть кирпичей
                    </Button>
                </Form.Item>
            </Form>
        </Row>
    )

}
