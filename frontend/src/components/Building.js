import {Button, DatePicker, Form, Icon, Input, Row} from 'antd';
import React, {useState} from 'react';


export default function Building(props) {

    const [dateOfBuild, handleDateOfBuild] = useState(null);

    function handleSubmit(e) {
        e.preventDefault();
        let address = e.target.buildAddress.value;
        let date = dateOfBuild;
        console.log(address);
        fetch('http://localhost:5000/building', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify({'date_of_build': date, 'address': address}),
            mode: 'no-cors'
        })
            .then(resp => resp.json())
            .then(resp => console.log(resp.json()))
    }

    function saveDate(date, dateString) {
        handleDateOfBuild(dateString);
        console.log(dateString, dateOfBuild);
    }

    return (
        <Row>
            <Form layout="inline" onSubmit={handleSubmit}>
                <Form.Item required>
                    <Input
                        name="buildAddress"
                        prefix={<Icon type="home" style={{color: 'rgba(0,0,0,.25)'}}/>}
                        placeholder="Адрес"
                    />
                </Form.Item>
                <Form.Item name="dateBuild" required>
                    <DatePicker onChange={saveDate}/>
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        Добавить адрес
                    </Button>
                </Form.Item>
            </Form>
        </Row>
    )
}