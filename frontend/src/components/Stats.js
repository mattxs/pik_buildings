import React from 'react';
import {Col, Row, Table} from 'antd';


export default function Stats(props) {
    const {dataSource} = props;
    console.log('stat', dataSource);

    const columns = [
        {
            title: 'Адрес',
            dataIndex: 'address',
            key: 'address',
        },
        {
            title: 'Количество кирпичей',
            dataIndex: 'bricks',
            key: 'bricks',
        },
        {
            title: 'Год постройки',
            dataIndex: 'date_of_build',
            key: 'date_of_build',
        },
    ];


    return (
        <Row>
            <Col span={5}/>
            <Col span={14}>
                <Table dataSource={dataSource} columns={columns}/>
            </Col>
            <Col span={5}/>
        </Row>
    )
}
